<?php
//Debug setting
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//DB Conf Vars

define("HOSTNAME", "localhost");
define("USERNAME", "bidalgo_user");
define("PASSWORD", "B1D4Lg0");
define("DBNAME", "bidalgo");

//Perform the connection first, since mysql_real_escape_string() require DB connection
$conn = new mysqli(HOSTNAME, USERNAME, PASSWORD, DBNAME); // Connect to bidalgo db
// Check if we are connected
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$commitMessage = "not provided";
if ($_FILES['uploadedfile']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['commit_message']['tmp_name'])) {
    $commitMessage = file_get_contents($_FILES['commit_message']['tmp_name']);
}

//Handle REQ GET params
$commitId = $conn->real_escape_string(isset($_GET["commit_id"]) ? $_GET["commit_id"] : 'not provided');
$commitTime = $conn->real_escape_string(isset($_GET["commit_time"]) ? $_GET["commit_time"] : 'not provided');
$commitUser = $conn->real_escape_string(isset($_GET["commit_user"]) ? $_GET["commit_user"] : 'not provided');

$sql = "INSERT INTO commits (commit_id, commit_time, commit_message, commit_user)
VALUES ('$commitId', '$commitTime', '$commitMessage', '$commitUser')";


if ($conn->query($sql) === TRUE) {
    echo "Thanks for your commit to Biadlgo Test Repo!";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>